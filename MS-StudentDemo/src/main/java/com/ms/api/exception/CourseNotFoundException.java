package com.ms.api.exception;

public class CourseNotFoundException extends RuntimeException {
	
	public CourseNotFoundException() {
		super("CourseNotFoundException");
	}

}
