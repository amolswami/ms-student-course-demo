package com.ms.api.exception;

public class NoCourseFoundException extends RuntimeException {
	public NoCourseFoundException() {
		super("NoCourseFoundException");
	}

}
