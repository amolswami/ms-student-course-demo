package com.ms.api.exception;

public class StudentNotFoundException extends RuntimeException {
	public StudentNotFoundException() {
		super("StudentNotFoundException");
	}

}
