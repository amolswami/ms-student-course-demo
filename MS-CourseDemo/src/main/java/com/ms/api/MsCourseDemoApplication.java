package com.ms.api;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.netflix.eureka.EnableEurekaClient;

@SpringBootApplication
@EnableEurekaClient
public class MsCourseDemoApplication {

	public static void main(String[] args) {
		SpringApplication.run(MsCourseDemoApplication.class, args);
	}

}
