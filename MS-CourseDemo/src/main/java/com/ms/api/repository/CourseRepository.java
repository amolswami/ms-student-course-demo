package com.ms.api.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.ms.api.model.Course;

public interface CourseRepository extends JpaRepository<Course, Long> {
	public Course findByTitle(String title);

}
