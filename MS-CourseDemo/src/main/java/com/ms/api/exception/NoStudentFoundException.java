package com.ms.api.exception;

public class NoStudentFoundException extends RuntimeException {
	public NoStudentFoundException() {
		super("NoStudentFoundException");
	}

}
