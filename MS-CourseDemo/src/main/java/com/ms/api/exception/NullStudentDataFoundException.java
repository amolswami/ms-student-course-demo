package com.ms.api.exception;

public class NullStudentDataFoundException extends RuntimeException {
	public NullStudentDataFoundException() {
		super("NullStudentDataFoundException");
	}

}
